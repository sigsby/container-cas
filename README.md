# CAS Container

Base container for Davenport University CAS deployment.

# Using Python Invoke

This project is managed is the [python invoke](https://www.pyinvoke.org/) tool,
a python-based task runner similar to GNU Make, Apache Ant, Ruby, Rake, etc.

On Fedora 34 you can install invoke using:
```bash
sudo dnf install python3-invoke
```

You will need to copy the file `invoke.sample.yml` to `invoke.yml` and make sure any missing variables are provided there.

You can see the tasks available using the command `invoke --list`.

A typical workflow:
```bash
invoke init build run
```

This will run in the foreground and display the live CAS server logs, so any
subsequent commands will need to be run in in a new terminal.

You can make changes to the files in `volumes/etc` and reload the config using:
```bash
invoke reload
```

You can run the project tests using:
```bash
invoke test
```

You can access a shell inside the container using:
```bash
invoke login
```

finally, you can publish the built container to the registry:
```bash
invoke login push logout
```
