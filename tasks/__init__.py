
import invoke, yaml, pprint
yaml.warnings({'YAMLLoadWarning': False})

from invoke import task

# @task
# def debug(c):
#   pprint.pprint(c.vars)

@task
def clean(c):
  """
  Remove downloaded project files
  """
  c.run('rm -rf overlay/')

@task
def init(c):
  """
  Download the CAS overlay project
  """
  deps = ','.join(c.vars.dependencies)
  cmd = (
  f'curl https://casinit.herokuapp.com/starter.tgz'
  f' -d type=cas-overlay'
  f' -d baseDir=overlay'
  f' -d casVersion="{c.vars.casVersion}"'
  f' -d bootVersion="{c.vars.bootVersion}"'
  f' -d dependencies="{deps}"'
  f' | tar -xzvf -'
  )
  c.run(cmd)
  c.run('cp overlay/etc/cas/config/cas.properties volumes/etc/config/cas.dist.properties')
  c.run('cp overlay/etc/cas/config/log4j2.xml volumes/etc/config/log4j2.dist.xml')

@task
def build(c):
  """
  Build the container image
  """
  with c.cd('./overlay'):
    c.run("./gradlew build jibDockerBuild")

@task
def run(c):
  """
  Run the built container in the foreground
  """
  cmd=(
  f'podman run -i --rm --name={c.vars.runName}'
  f' --volume=./volumes/etc:/etc/cas:z'
  f' --volume=./volumes/log:/var/log/cas:z'
  f' --publish=8443:8443'
  f' localhost/apereo/cas:{c.vars.casVersion}'
  )
  c.run(cmd)

@task
def reload(c):
  """
  Reload the running container config
  """
  cmd=(
  f'curl -sk -X POST https://localhost:8443/cas/actuator/refresh'
  )
  c.run(cmd)

@task
def shell(c):
  """
  Access a shell prompt in the container
  """
  c.run(f'podman exec -it {c.vars.runName} /bin/sh', echo_stdin=False)

@task
def test(c):
  """
  Run the project tests
  """
  c.run('./test/run -v')

@task
def login(c):
  """
  Login to the container registry
  """
  cmd=(
  f'podman login --tls-verify=false'
  f' -u=\'{c.vars.pushUser}\''
  f' -p=\'{c.vars.pushPass}\''
  f' {c.vars.pushHost}'
  )
  c.run(cmd)

@task
def logout(c):
  """
  Logout of the container registry
  """
  cmd=(
  f'podman logout {c.vars.pushHost}'
  )
  c.run(cmd)

@task
def push(c):
  """
  Push the container image to the container registry
  """
  cmd=(
  f'podman push --tls-verify=false'
  f' localhost/apereo/cas:{c.vars.casVersion}'
  f' {c.vars.pushHost}{c.vars.pushPath}/{c.vars.casVersion}:{c.vars.pushVersion}'
  )
  c.run(cmd)
